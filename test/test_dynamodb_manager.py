import unittest
import hdxpyutils

hdxpyutils.set_log_level('INFO')

TABLE_NAME = 'hdx-dev-testing-table'

class DynamodbManagerTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.ddbm = hdxpyutils.DynamodbManager()
    
    def test_pql_query(self):
        data = self.ddbm.pql_query(f'SELECT * FROM "{TABLE_NAME}";')
        self.assertTrue('items' in data)
    
    # def test_query(self):
    #     data = self.ddbm.query(
    #         'test-dev', 
    #         IndexName='gsi_opening_date',
    #         KeyConditionExpression=f'opening_date = :opening_date', 
    #         FilterExpression='#file = :filetype',
    #         ExpressionAttributeNames={ '#file': 'filetype' },
    #         ExpressionAttributeValues={ ':filetype': { 'S': 'EMPF'}, ':opening_date': {'S': '2021-01-01'} },
    #         ScanIndexForward=False
    #     )
    #     self.assertTrue('items' in data)
    
    # def test_scan(self):
    #     #data = self.ddbm.scan(TABLE_NAME)
    #     data = self.ddbm.scan(
    #         'test-dev', 
    #         Select='COUNT',
    #         FilterExpression='#file = :filetype',
    #         ExpressionAttributeNames={ '#file': 'file' },
    #         ExpressionAttributeValues={ ':filetype': { 'S': 'EMPF'} },
    #     )
    #     print(data)
    #     self.assertTrue('items' in data)
    
    def test_put_item(self):
        r = self.ddbm.put_item(TABLE_NAME, {'id': 'myitem', 'name': 'Test Item'})
        self.assertTrue(r)
    
    def test_get_item(self):
        item = self.ddbm.get_item(TABLE_NAME, {'id': 'myitem'})
        self.assertTrue(item == {'id': 'myitem', 'name': 'Test Item'})

    def test_delete_item(self):
        r = self.ddbm.delete_item(TABLE_NAME, {'id': 'myitem'})
        self.assertTrue(r)
        



    
    
    
    
