import unittest
import hdxpyutils

hdxpyutils.set_log_level('INFO')

class InoaApiManagerTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):       
        secrets_manager = hdxpyutils.SecretsManager()
        secret = secrets_manager.get_secret('inoa-credentials')

        self.iam = hdxpyutils.InoaApiManager(
            api_url=secret['inoa_url'],
            username=secret['username'], 
            password=secret['password']
        )

    def test_query(self):
        funds = self.iam.call('funds', 'get_funds')
        self.assertTrue(len(funds) > 0)