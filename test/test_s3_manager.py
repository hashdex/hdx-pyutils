import unittest
import hdxpyutils
import pandas as pd

hdxpyutils.set_log_level('INFO')

BUCKET = 'hdx-dev-testing-bucket'

class S3ManagerTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.s3m = hdxpyutils.S3Manager()
    
    def test_save_to_s3(self):
        r = self.s3m.save_to_s3(BUCKET, 'id,name\r\nmyid,hello', 'hello.csv')
        self.assertTrue(r['ResponseMetadata']['HTTPStatusCode'] == 200)
    
    def test_save_df_to_s3(self):
        df = pd.DataFrame([{'id': 'test', 'age': 20}])
        r = self.s3m.save_df_to_s3(BUCKET, df, 'df.csv')
        self.assertTrue(r['ResponseMetadata']['HTTPStatusCode'] == 200)
    
    def test_list_files(self):
        items = self.s3m.list_files(BUCKET)
        self.assertTrue(type(items) == list)
    
    def test_file_exists(self):
        r = self.s3m.file_exists(BUCKET, 'df.csv')
        self.assertTrue(type(r) == bool)

    def test_read_csv(self):
        r = self.s3m.read_csv(BUCKET, 'hello.csv')
        self.assertTrue(type(r) == pd.DataFrame)