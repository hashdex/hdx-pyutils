import unittest
import hdxpyutils

hdxpyutils.set_log_level('INFO')

class DatalakeManagerTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.lm = hdxpyutils.LambdaManager()

    def test_query(self):
        result = self.lm.invoke('function-name')
        self.assertTrue(result['statusCode'] == 200)