import unittest
import hdxpyutils

hdxpyutils.set_log_level('INFO')

class InoaApiManagerTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.ipam = hdxpyutils.InoaPortApiManager('https://hdx-inoa-port.prod.hashdex.io')

    def test_query(self):
        funds = self.ipam.get_funds()
        self.assertTrue(len(funds) > 0)