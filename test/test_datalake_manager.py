import unittest
import hdxpyutils

hdxpyutils.set_log_level('INFO')

class DatalakeManagerTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        secrets_manager = hdxpyutils.SecretsManager()
        secret = secrets_manager.get_secret('athena-credentials')

        self.dm = hdxpyutils.DatalakeManager(
            key=secret['key'], 
            secret=secret['secret'], 
            s3_staging_dir='s3://hdx-datalake/data/stg/'
        )

    def test_query(self):
        data = self.dm.query(''' SELECT * FROM "data-lake-raw"."filecoin_status" limit 10 ''')
        self.assertTrue(data.shape and data.shape[0] > 0)