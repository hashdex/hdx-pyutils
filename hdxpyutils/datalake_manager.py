import os
import pandas as pd
from pyathena import connect
import logging
logger = logging.getLogger(__name__)

class DatalakeManager():
  def __init__(self, key:str, secret:str, s3_staging_dir:str, region:str='us-east-1'):
    ''' DatalakeManager Constructor.

        @param key: AWS key.
        @param secret: AWS secret.
        @param s3_staging_dir: S3 staging directory.
        @param region: AWS Region. 
    '''

    if 's3://' not in s3_staging_dir:
      raise Exception(f's3_staging_dir has received an invalid s3 path: {s3_staging_dir}') 

    logger.info('Creating instance of Datalake Client.')

    self.connection = connect(
      aws_access_key_id=key, 
      aws_secret_access_key=secret, 
      s3_staging_dir=s3_staging_dir, 
      region_name=region
    )

  def query(self, sql_string:str):
    ''' Makes SQL query to Athena service.

        @param sql_string: SQL query.
        @returns pandas.DataFrame.
    '''

    logger.info(f'Sending query to Athena. ({sql_string})')

    df = pd.read_sql(sql_string, self.connection)

    logger.info(f'Dataframe size: {df.size}')
    logger.info(f'Dataframe head:')
    logger.info(df.head(2))

    return df