import requests, requests.auth
import logging
logger = logging.getLogger(__name__)

class InoaApiManager():
  def __init__(self, api_url:str, username:str, password:str):
    ''' InoaApi Constructor.

        @param api_url (string): INOA API path.
        @param username (string): INOA Username.
        @param password (string): INOA Password.
    '''

    self.api_path = '%s/api/2/sync' % (api_url)
    self.auth = requests.auth.HTTPBasicAuth(username, password)
    logger.info('Creating instance of INOA API Client.')
    
  def call(self, module:str, method:str, params:dict = {}):
    ''' Makes request to inoa server. 
    
        @param module (string): Module name.
        @param method (string): Method name.
        @param params (dict): Request params.
    '''

    endpoint = f'{self.api_path}/{module}/{method}'

    logger.info(f'Sending request to {endpoint}')

    r = requests.post(endpoint, json=params, auth=self.auth)
  
    logger.info(f'Response status code: {r.status_code}')
    logger.info(f'Response content (truncated): {r.content[:100]}...')
    
    r.raise_for_status()
    response = r.json()
    return response