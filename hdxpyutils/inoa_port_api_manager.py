import requests
import logging
logger = logging.getLogger(__name__)

class InoaPortApiManager():
  def __init__(self, inoa_port_url:str):
    ''' InoaPortApi Constructor.

        @param inoa_port_url: INOA Port API path.
    '''

    logger.info('Creating instance of INOA Port API Client.')

    self.api_path = inoa_port_url

  def get(self, endpoint:str, params:dict={}):
    logger.info(f'Sending GET request to {self.api_path}{endpoint}')
    logger.info(f'With params: {params}')

    r = requests.get(f'{self.api_path}{endpoint}', params=params)

    logger.info(f'Response status code: {r.status_code}')
    logger.info(f'Response content (truncated): {r.content[:100]}...')

    r.raise_for_status()
    return r.json()
  
  def build_params(self, params:dict={}):
    p = {}

    for param in params:
      if params[param] is not None:
        p[param] = params[param]

    if 'cnpjs' in p:
      if not isinstance(p['cnpjs'], list):
        raise Exception('Paramater cnpjs must be a list.')
      p['cnpjs'] = ','.join(p['cnpjs'])
    
    return p

  def get_currencies(self):
    return self.get('/currencies')
  
  def get_exposures_by_asset(self, cnpjs:list=None, start_date:str=None, end_date:str=None):
    params = self.build_params({'cnpjs': cnpjs, 'start_date': start_date, 'end_date': end_date})
    return self.get('/exposures-by-asset', params)
  
  def get_feeders_positions(self, cnpjs:list=None, start_date:str=None, end_date:str=None):
    params = self.build_params({'cnpjs': cnpjs, 'start_date': start_date, 'end_date': end_date})
    return self.get('/feeders/positions', params)

  def get_funds(self, cnpjs:list=None):
    params = self.build_params({'cnpjs': cnpjs})
    return self.get('/funds', params)

  def get_instruments(self, cnpjs:list=None):
    params = self.build_params({'cnpjs': cnpjs})
    return self.get('/instruments', params)

  def get_movements(self, cnpjs:list=None, start_date:str=None, end_date:str=None):
    params = self.build_params({'cnpjs': cnpjs, 'start_date': start_date, 'end_date': end_date})
    return self.get('/movements', params)

  def get_navs(self, cnpjs:list=None, start_date:str=None, end_date:str=None):
    params = self.build_params({'cnpjs': cnpjs, 'start_date': start_date, 'end_date': end_date})
    return self.get('/navs', params)

  def get_quotations(self, cnpjs:list=None, start_date:str=None, end_date:str=None):
    params = self.build_params({'cnpjs': cnpjs, 'start_date': start_date, 'end_date': end_date})
    return self.get('/quotations', params)

  def get_rebate_rates(self, cnpjs:list=None, start_date:str=None, end_date:str=None):
    params = self.build_params({'cnpjs': cnpjs, 'start_date': start_date, 'end_date': end_date})
    return self.get('/rebate-rates', params)

  def get_related_parties(self):
    return self.get('/related-parties')

  def get_rentabilities(self, cnpjs:list=None, start_date:str=None, end_date:str=None):
    params = self.build_params({'cnpjs': cnpjs, 'start_date': start_date, 'end_date': end_date})
    return self.get('/rentabilities', params)

  def get_shareholders_history(self, cnpjs:list=None, date:str=None):
    params = self.build_params({'cnpjs': cnpjs, 'date': date})
    return self.get('/shareholders/history', params)

  def get_shareholders_positions(self, cnpjs:list=None, date:str=None):
    params = self.build_params({'cnpjs': cnpjs, 'date': date})
    return self.get('/shareholders/positions', params)