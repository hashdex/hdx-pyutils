from hdxpyutils.secrets_manager import SecretsManager
from hdxpyutils.datalake import DatalakeClient
from hdxpyutils.inoa import InoaApiClient, InoaPortApiClient

secrets_manager = SecretsManager(log=True)
secret = secrets_manager.get_secret('hdx-sagemaker-secret')


client = DatalakeClient(key=secret['athena_key'], secret=secret['athena_secret'], log=True)
data = client.query(''' SELECT * FROM "data-lake-raw"."filecoin_status" limit 10 ''')


api = InoaApiClient(api_url=secret['inoa_url'], username=secret['inoa_username'], password=secret['inoa_password'], log=True)
instruments = api.call('instruments', 'get_instruments')


inoa_port_api_client = InoaPortApiClient('https://hdx-inoa-port.prod.hashdex.io', log=True)
instruments = inoa_port_api_client.get_instruments()

